<?php
use jream\MVC\Controller, jream\Session, jream\Hash;

class Youtube extends Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function search($term, $startIndex = 1, $total = 20)
	{
		/* display a list of videos */ 
		$searchTerm = $term;
		$yt = new Zend_Gdata_YouTube();
		$query = $yt->newVideoQuery();
		$query->setQuery($searchTerm);
		$query->setStartIndex($startIndex);
		$query->setMaxResults($total);
		// $query->setFeedType('most viewed');
		$query->setTime('this_week');
		$feed = $yt->getVideoFeed($query);
		$this->outputVideoList($feed);
	}
	
	public function findFlashUrl($entry)
	{
		$this->model->findFlashUrl($entry);
	}

	public function getTopRatedVideosByUser($user)
	{
		$this->model->getTopRatedVideosByUser($user);
	}

	public function getRelatedVideos($videoId)
	{
		$this->model->getRelatedVideos($videoId);
	}
	
	public function outputThumbnails($feed)
	{
		$this->model->outputThumbnails($feed);
	}	
	
	public function outputVideoPlayer($videoId)
	{
		$this->model->outputVideoPlayer($videoId);
	}
	
	public function outputVideoMetadata($entry)
	{
		$this->model->outputVideoMetadata($entry);
	}
	
	public function outputVideoList($feed)
	{
		$this->model->outputVideoList($feed);
	}

}