<?php
use jream\MVC\Controller, jream\Session, jream\Hash;

class Home extends Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$this->view->render('inc/header', array('title' => 'Home'));
		$this->view->render('home/home');
		$this->view->render('inc/footer');
	}
	
	public function search($query, $startIndex, $total)
	{
		$cache = new jream\Cache();
		$cacheData = $cache->exists(sha1($query));
		
		if ($cacheData == false || time() > $cacheData['time'])
		{
			$yt = $this->loadModel('youtube');
			$result = $yt->search($query, $startIndex, $total);
			$cache->save(sha1($query), json_encode($result));
		}
		else
		{
			$result = json_decode($cacheData['data']);			
		}
		
		jream\Output::success($result);
	}
	
	public function videoPlayer($service)
	{
		if ($service == 'youtube') {
			$yt = $this->loadModel('youtube');
			$result = $yt->outputVideoPlayer('O9KzAyD6hPw');
			jream\Output::success($result);
		}
	}
	
	public function download()
	{
		$url = $_POST['url'];
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		
		/**
		http://o-o---preferred---dfw06s14---v23---lscache6.c.youtube.com/videoplayback?ip=70.119.60.58&upn=OXo6rENOf6U&sparams=algorithm%2Cburst%2Ccp%2Cfactor%2Cgcr%2Cid%2Cip%2Cipbits%2Citag%2Csource%2Cupn%2Cexpire&fexp=924804%2C910104%2C910207%2C915507%2C907217%2C922401%2C919804%2C920704%2C912806%2C906055%2C924500%2C906831%2C925701%2C924700%2C911406%2C913550%2C904721%2C920706%2C907344%2C912706&mt=1344991991&key=yt1&algorithm=throttle-factor&burst=40&ipbits=8&itag=35&sver=3&signature=2B1D1BA55B328B37E1CC99FD369E2E8269B58849.40BA8D150E6894FA4EA0D1E3A17F22820C0119FA&mv=m&source=youtube&ms=au&gcr=us&expire=1345015023&factor=1.25&cp=U0hTSlBMU19HTUNOM19IRldBOjl5YjZlbE5zNUha&id=88b3de3f1aeab33e&newshard=yes&title=PARTY%20HARD%21

		 http:\/\/o-o---preferred---dfw06s14---v23---lscache6.c.youtube.com\/generate_204?ip=70.119.60.58\u0026upn=qOxBmLebq-E\u0026sparams=algorithm%2Cburst%2Ccp%2Cfactor%2Cgcr%2Cid%2Cip%2Cipbits%2Citag%2Csource%2Cupn%2Cexpire\u0026fexp=924804%2C910104%2C910207%2C915507%2C907217%2C922401%2C919804%2C920704%2C912806%2C906055%2C924500%2C906831%2C925701%2C924700%2C911406%2C913550%2C904721%2C920706%2C907344%2C912706\u0026mt=1344991691\u0026key=yt1\u0026algorithm=throttle-factor\u0026burst=40\u0026ipbits=8\u0026itag=34\u0026sver=3\u0026signature=9B9C3DC2B1DDB36AFD73AD7F8098761503A3F6FE.0A41A5045177ECB1A1D106F2ADF9AE4C91AB5432\u0026mv=m\u0026source=youtube\u0026ms=au\u0026gcr=us\u0026expire=1345015023\u0026factor=1.25\u0026cp=U0hTSlBMU19HTUNOM19IRldBOjl5YjZlbE5zNUha\u0026id=88b3de3f1aeab33e\u0026newshard=yes
		 */
		
		$r = preg_match_all('/(yt.preload.start)/', $response, $matches);
		if ($r == true) {
			print_r($matches);
		}
		
		
		echo $response;
		
	}
}