<?php
/**
 * @author		Jesse Boyer <contact@jream.com>
 * @copyright	Copyright (C), 2011-12 Jesse Boyer
 * @link		http://jream.com
 */

use jream\Registry, 
	jream\Autoload, 
	jream\Database, 
	jream\Hash, 
	jream\MVC\Bootstrap,
	jream\MVC\Route,
	jream\Session;

/**
 * Configuration Timezone 
 */
date_default_timezone_set ('UTC');
define('DATETIME', date('Y-m-d H:i:s', strtotime('now')));

/**
 * Init the System
 */
require_once 'library/jream/autoload.php';
new Autoload('library/jream/');

/**
 * Start the Session 
 */
Session::start();

/**
 * Init Settings
 */
define('HASH_KEY', 'dk$fkae?423$kah%3jkAF4#@Ls');
if (!isset($_SESSION['csrf'])) $_SESSION['csrf'] = Hash::create('sha256', rand(10000, 99999), HASH_KEY);

/**
 * Database
 */
$db = array(
	'type' => 'mysql'
	,'host' => 'localhost'
	,'name' => 'cliplemon'
	,'user' => 'root'
	,'pass' => ''
);

Registry::set('db', new Database($db));

/**
 * Init Routes for custom routes
 */
$route = new Route(array(
));


/**
* Load Zend Data
*/
ini_set('include_path', ini_get('include_path') . PATH_SEPARATOR . dirname(__FILE__). DIRECTORY_SEPARATOR. 'library');
require_once 'library/Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance();

/**
 * Init MVC
 */
$bootstrap = new Bootstrap();

/**
 *See if there is a route match 
 */
$reroute = $route->match($bootstrap->uri);
// For live site
//$bootstrap->setPathRoot('/' . getcwd());
// For local site
$bootstrap->setPathRoot(getcwd());
$bootstrap->setPathController('controller/');
$bootstrap->setPathModel('model/');
$bootstrap->setPathView('view/');
$bootstrap->setControllerDefault('home');
$bootstrap->init($reroute);


define('BASE_URI', $bootstrap->uriSlashPath);

/**
 * URI Segments (array), If they are needed 
 */
Registry::set('segments', $bootstrap->uriSegments);

