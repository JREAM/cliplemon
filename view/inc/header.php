<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>clip LEMON | <?php echo $this->title; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<link href="<?php echo BASE_URI;?>public/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo BASE_URI;?>public/css/style.css" rel="stylesheet">
	<link href="<?php echo BASE_URI;?>public/css/bootstrap-responsive.css" rel="stylesheet">

	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<script src="<?php echo BASE_URI;?>public/js/jquery.js"></script>
	<script src="<?php echo BASE_URI;?>public/js/jquery-ui.js"></script>
	<script src="<?php echo BASE_URI;?>public/js/bootstrap.js"></script>
	<script src="<?php echo BASE_URI;?>public/js/cliplemon/search.js"></script>
	<script src="<?php echo BASE_URI;?>public/js/cliplemon/download.js"></script>
	<script>
	var BASE_URI = '<?=BASE_URI?>';
	</script>
</head>
<body>

	
<div class="container" id="main">
	
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container">
				<span class="brand"><img src="<?=BASE_URI?>public/img/logo.png" alt="clip Lemon" /></span>
			</div>
		</div>
	</div>
