<div class="span12">
	
	<div class="row span3">
		<p>Search for Videos!</p>
		<form id="searchForm" action="#">
			<input id="search" type="text" placeholder="Query" />
			<input type="submit" class="btn btn-primary" />
			
		</form>

		<p>Playlist</p>
		<div id="playlist">Drag Files to Create Playlist</div>
		
		<p>Download Video</p>
		
		<form id="downloadForm" action="#">
			<input id="downloadUrl" type="text" placeholder="URL" />
			<input type="submit" class="btn btn-primary" />
		</form>
		
	</div>
	
	<div class="row span8">
		<div id="searchResult"></div>
	</div>
	
  
</div>

<div class="modal hide" id="player">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">x</button>
		<h3></h3>
	</div>
	<div class="modal-body">
	<!-- Dynamic -->
	</div>
</div>