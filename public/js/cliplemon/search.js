$(function() {

	$("#playlist").droppable();

	$.storage = {
		query: 'lemon',
		startIndex: 1,
		total: 20,
		ajaxLoader: '<img src="public/img/ajax-loader.gif" alt="Loading" />'
	};
	
	search();
	
	$("#searchForm").submit(function(e) {
		e.preventDefault();
		$.storage.query = $("#search").val();		
		search();
	});
	
	$(".paginationPage").live('click', function(e) {
		e.preventDefault();
		$.storage.startIndex = $(this).attr('rel');
		search();
	});

	$(".preview").live('click', function(e) {
		e.preventDefault();
		
		var url = $(this).attr('href');
		var title = $(this).attr('title');
		
		var player = "<object width='700' height='450'>";
		player += "<param name='movie' value='"+url+"&autoplay=1'></param>";
		player += "<param name='wmode' value='transparent'></param>";
		player += "<embed src='"+url+"&autoplay=1' type='application/x-shockwave-flash' wmode='transparent' width='700' height='450'></embed>";
		player += "</object>";	
		
		var dom = $(this).parents('div.videoListing').find('.player');
		
		dom.html($.storage.ajaxLoader);
		dom.html(player);
	});
	
	$(".preview").live('hover', function(e) {
		e.preventDefault();
		$(this).children('img').attr('width', '150');		
	});
	$(".preview").live('mouseout', function(e) {
		e.preventDefault();
		$(this).children('img').attr('width', '100');		
	});
	
});

		
function search(startIndex, total) {
	/**
	* Prevent an empty query
	*/
	if ($.storage.query.length == 0) {
		return false;
	}
	
	$("#searchResult").html($.storage.ajaxLoader);
			
	$.get(BASE_URI + 'home/search/' + encodeURI($.storage.query) + '/' + $.storage.startIndex + '/' + $.storage.total, function(o) {
		if (o.success == 1) {
			var output = '';
			for (var i = 0; i < o.data.length; i++) {
				output += '<div class="videoListing">';
				output += '<div class="videoDesc"><h3>'+o.data[i].title+'</h3><p>'+o.data[i].description.substr(0, 200)+'</p> <p><a class="preview" href="'+o.data[i].flashUrl+'">[ View ]</a></p></div>';
				output += '<div class="videoThumb">';
				output += '<a class="preview" href="'+o.data[i].flashUrl+'" title="'+o.data[i].title+'">';
				output += "<img width='100' src='"+o.data[i].thumbnailUrl+"' />";
				output += '</a>';
				output += '</div>';
				output += '<div class="clear"></div>';
				
				output += '<div class="player"></div>';
				output += '</div>';
				
				output += '<div class="clear"></div>';
			}
			
			output += '<div id="pagination">';
			for (var j = 1; j < 20; j++) {
				if ($.storage.startIndex == j) {
					output += '<b><a class="paginationPage btn btn-primary btn-min" rel="'+j+'" href="#">'+j+'</a></b> ';
				} else {
					output += '<a class="paginationPage btn btn-min" rel="'+j+'" href="#">'+j+'</a> ';
				}
			}
			output += '</div>';
			
			$('.videoThumb').draggable();
			
			$("#searchResult").html(output);
		} else {
			$("#searchResult").html('Error');
		}
	}, 'json');
}
