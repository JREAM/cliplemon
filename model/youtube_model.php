<?php
use jream\MVC\Model, jream\Session, jream\Hash;

class Youtube_Model extends Model {

	public function search($term, $startIndex = 1, $total = 20)
	{
		$yt = new Zend_Gdata_YouTube();
		$query = $yt->newVideoQuery();
		$query->setQuery($term);
		$query->setStartIndex($startIndex);
		$query->setMaxResults($total);
		// $query->setFeedType('most viewed');
		$query->setTime('this_week');
		$feed = $yt->getVideoFeed($query);
		
		$output = array();
		
		foreach ($feed as $key => $entry) 
		{		
			$output[$key]['videoId'] = $entry->getVideoId();
			$output[$key]['thumbnailUrl'] = $entry->mediaGroup->thumbnail[0]->url;
			$output[$key]['title'] = (string) $entry->mediaGroup->title;
			$output[$key]['description'] = (string) $entry->mediaGroup->description;
			
			foreach ($entry->mediaGroup->content as $content) 
			{
				if ($content->type === 'application/x-shockwave-flash') 
				{
					$output[$key]['flashUrl'] = $content->url;
				}
			}
			
		}
		
		return $output;
	}

	/**
	 * output video metadata
	 *
	 * @param  Zend_Gdata_YouTube_VideoEntry $entry The video entry
	 * @return array
	 */
	public function outputVideoMetadata($entry)
	{
		$title = $entry->mediaGroup->title;
		$description = $entry->mediaGroup->description;
		$authorUsername = $entry->author[0]->name;
		$authorUrl = 'http://www.youtube.com/profile?user=' . $authorUsername;
		$tags = $entry->mediaGroup->keywords;
		$duration = $entry->mediaGroup->duration->seconds;
		
		$output = array();
		$output['title'] = $title;
		$output['description'] = $description;
		$output['authorUrl'] = $authorUrl;
		$output['tags'] = $tags;
		$output['duration'] = $duration;
		
		return $output;
	}

}